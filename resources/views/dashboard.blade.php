<x-app-layout>

        @if (!auth()->user()->profile_complete)

        <div class="completeprofile bg-white mt-4">
            <h1>Welcome!</h1>
            <h1>Please finish up your profile on the following <a href="{{ route('profiles.edit',['profile' => auth()->id()]) }}" class="link">link</a> so that you can enjoy all our features.</h1>
        </div>
        @endif



    <div class="row mt-5 mx-3">
        <div class="dashboardtitle d-flex flex-row justify-between my-4 mr-4">
            <h2 class="text-start">In what field can you be amazing?</h2>
            <div class="d-flex"><img src="../icons/3.png" alt="" width="50px" class="rightarrow"><span class="mr-4">Checkout the latest projects</span></div>


        </div>

        <div class="col-lg-2 offset-lg-1 mb-4">
                <div class="d-grid grid-cols-1 gap-2 mb-3">

                @foreach ($academies as $academy)

                        <a href="{{route('ajax.projects', ['academy_id' =>$academy->id] )}}" class="btn btn-outline-dark filter-btn py-3 filterbtn">
                            {{ $academy->name }}
                        </a>
                @endforeach
                <a href="{{route('dashboard')}}" class="btn btn-outline-dark all py-3 filterbtn">
                    ALL
                    </a>
            </div>
        </div>
        <div id="projectsContainer" class="col-lg-7 mx-auto">

        </div>
    </div>

    @section('js')
            <script>
                 $(document).ready(function(e) {
                let url = '{{ route("ajax.projects") }}';

                $.get(url).then(function(result){
                $('#projectsContainer').html(result);
                })

                $(document).on('click', '.filter-btn', function(e) {

                    e.preventDefault();

                    let url = $(this).attr('href');

                    $.get(url).then(function(result) {
                        $('#projectsContainer').html(result);
                    })

                })

                $(document).on('click', '.filter-btn', function(e) {

                    e.preventDefault();

                    $('#projectsContainer').html(result);

                })
                $(function() {
                    $('.all').focus();
                });

            })



            </script>

    @endsection

</x-app-layout>
