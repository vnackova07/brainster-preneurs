@foreach ($projects as $project)
    @include('inc.project-card')
@endforeach


<div id="pagination" class="mb-5 text-black">
    {{ $projects->links() }}
</div>

<script>


    $('#pagination').find('a').on('click', function(e) {
        e.preventDefault();

        let url = $(this).attr('href');

        $.get(url).then(function(result) {
            $('#projectsContainer').html(result);
        })
    })



</script>
