<div class="wholecard">

<div class="card mb-5">

    <div class="card-body">
        <div class="d-flex justify-content-between align-items-start">

            @if ($project->user->avatar)
                <img class="img-thumbnail" width="90px" src="{{  url('assets/profile/'.$project->user->avatar) }}" alt="User image">
            @else
                <img class="img-thumbnail" width="90px"
                    src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png" alt="User image">
            @endif

            <a href="{{ route('applicants.index', $project->id) }}"
                class="btn btn-outline-dark count {{ Request::segment(1) == 'ajax' || Request::segment(1) == 'applications' ? 'disabled' : '' }}">
                {{ $project->applicants->count() }} <p> Applicants</p>
            </a>

        </div>
        <div class="row">
            <div class="col-md-2 text-center">
                <p class="mt-2 fw-bold mb-2">
                    {{ $project->user->name }}
                </p>

                <small class="textyellow">I am a</small>
                <small class="textyellow"> {{ $project->user->academy->profession }} </small>
                <br>
                <br>
                <small class="lookingfor">I'm looking for</small>
            </div>
            <div class="col-md-10">
                <h1 class="projectname">{{ $project->name }}</h1>

                <p class="showmore mt-5 ">{{ $project->description }}</p>

                <br>
                @if ($project->is_assembled)
                    @if (Request::segment(1) == 'projects')
                        <div class="d-flex justify-content-end">
                            <img width="30"
                                src="{{ asset('icons/Screenshot_2.png') }}"
                                alt="star">
                        </div>
                    @endif

                    @if (Request::segment(1) == 'applications')
                        @if ($project->applicants()->where('user_id', Auth::user()->id)->first()->pivot->status)
                            <small class="text-success d-flex text-bold">Application accepted <img src="{{ asset('icons/5.png') }}" alt="" width="20px" class="ml-2 text-center"></small>
                        @else
                            <small class="text-danger d-flex text-bold">Application denied <img src="{{ asset('icons/6.png') }}" alt="" width="20px" class="ml-2 text-center"></small>
                        @endif

                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <div>
            @foreach ($project->requirements as $requirement)
                <span class="badge bg-secondary semicircle">
                    {{ $requirement->name }}
                </span>
            @endforeach
        </div>
        @if (Request::segment(1) == 'ajax')

            <button action="{{ route('applications.index')}}" class="btn btn-dark px-5 mr-4 imin btn-selected im-in {{ auth()->user()->profile_complete ? '' : 'disabled' }}" iud="{{auth()->user()}}" cid = "{{ $project->id }}" >I'm In</button>

        @endif
    </div>


</div>



@if ($project->user_id == Auth::id())
<div class="editdelete">
    <a href="{{ route('projects.edit', ['project' => $project]) }}"><img src="../icons/8.png" alt="" width="30px"
        class="editicon"></a>
    <form method="POST" action="{{route('projects.destroy', $project->id)}}">
        @csrf
        @method("DELETE")
    <button type="submit" class="delete"><img src="../icons/7.png" alt="" width="30px" class="deleteicon"></button>
    </form>
</div>
@endif

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

    document.querySelectorAll('.showmore').forEach(element => {

        if (element.outerText.length > 500) {
            let span = document.createElement('span')
            span.className = 'show t-orange'
            span.innerText = 'show more'
            element.parentNode.appendChild(span)

            let tempText = String(element.outerText)
            element.innerText = String(element.innerText).slice(0, 500)
            $(span).on('click', () => {
                element.innerText = tempText
                $(span).remove()
            })
        }
    });



</script>
