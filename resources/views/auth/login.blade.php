<link rel="stylesheet" type="text/css" href="{{ url('css/style.css')}}">



<x-guest-layout>
    <div class="container bglogin flex flex-row">
        <div class="one">
            <h1 class="title">BRAINSTER<span>PRENEURS</span></h1>
            <p>Propel your ideas to life!</p>
        </div>   
        
        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        
    
        
    <div class="two">
        <h2 class="login">Login</h2>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            
            <!-- Email Address -->
        
            <div class="input">
                <x-label class = "d-none" for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full text-line" type="email" name="email" placeholder="Email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4 input">
                <x-label class = "d-none" for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full text-line"
                                type="password"
                                name="password"
                                placeholder="Password"
                                required autocomplete="current-password" />
            </div>
             <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <!-- Remember Me -->
            <!--<div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>-->

            <!--<div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif-->

                <x-button class="ml-3 mt-4 loginbtn">
                    {{ __('LOGIN') }}
                </x-button>

                <p>Don't have an account, register <a href="{{ route('register') }}">here!</a></p>
            </div>
            
        
        </form>
    </div>
   

</div>   
</x-guest-layout>

