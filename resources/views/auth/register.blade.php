<link rel="stylesheet" type="text/css" href="{{ url('css/style.css')}}">

<x-guest-layout>
        
        <div class="container bgregister register flex flex-row">
            <div class="one">
        <!-- Validation Errors -->
        <!--<x-auth-validation-errors class="mb-4" :errors="$errors" />-->
                <h1 class="registertitle">Register</h1>
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div class="row">
            <div class="two">
            <div class="input">
                <!--<x-label for="name" :value="__('Name')" />-->

                <x-input id="name" class="block mt-1 w-full text-line" type="text" name="name" placeholder="Name" :value="old('name')" autofocus />
                
            </div>
            @error('name')
                <div class="mt-1 ml-3 list-disc list-inside text-sm text-red-600">
                    {{ $message }}
                </div>
                    
            @enderror
        </div>

            <!-- Surname -->
            <div class="two">
            <div class="mt-4 input">
                <!--<x-label for="surname" :value="__('Surname')" />-->

                <x-input id="surname" class="block mt-1 w-full text-line" type="text" name="surname" placeholder="Surname" :value="old('surname')"/>
               
            </div>
            @error('surname')
            <div class="mt-1 ml-3 list-disc list-inside text-sm text-red-600">
                {{ $message }}
            </div>
                
            @enderror
            </div>
        </div>

            <!-- Email Address -->
            <div class="row">
            <div class="two">
            <div class="mt-4 input">
                <!--<x-label for="email" :value="__('Email')" />-->

                <x-input id="email" class="block mt-1 w-full text-line" type="email" name="email" placeholder="Email" :value="old('email')"/>

                
            </div>
            @error('email')
                <div class="mt-1 ml-3 list-disc list-inside text-sm text-red-600">
                    {{ $message }}
                </div>
                    
            @enderror
            </div>

            <!-- Password -->
            <div class="two">
            <div class="mt-4 input">
                <!--<x-label for="password" :value="__('Password')" />-->

                <x-input id="password" class="block mt-1 w-full text-line"
                                type="password"
                                name="password"
                                placeholder="Password"
                                autocomplete="new-password" />
                
            </div>
            @error('password')
                <div class="mt-1 ml-3 list-disc list-inside text-sm text-red-600">
                    {{ $message }}
                </div>
                    
                @enderror
            </div>
        </div>
                <h1 class="biography">Biography</h1>
                <div class="biotext">Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a type
                    specimen book. It has survived not only five centuries, but also
                    the leap into electronic typesetting, remaining essentially
                    unchanged.</div>
           

            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4 registerbtn">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
            </div>

            <div class="two1"></div>
        </div>
</x-guest-layout>
