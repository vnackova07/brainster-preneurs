<x-app-layout>
    <div class="container-fluid assembly-container">
    <div class="d-flex justify-content-between px-5">
        <div class="applicantsname flex-column">
            <h1 class="h3 pt-5">Name of Project - Applicants</h1>
            <h1 class="h3 pt-5 d-flex">Choose your teammates <img src="{{ asset('icons/4.png') }}" alt="" class="rightarrow mt-3" width="50px"></h1>
        </div>
        <div class="pt-5 d-flex flex-column text-center appleft">
            <small class="pb-2 smalltext">Ready to start?</small>
            <small class="pb-2 smalltext">Click on the button bellow</small>
            <a href="{{ route('applicants.assemble', $project->id) }}" class="btn d-flex assembly-button {{ $ready_for_assemblance ? '' : 'disabled' }}">Team Assembled <img src="{{ asset('icons/5.png') }}" alt="" width="25px" class="ml-2 text-center"></a>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-6 offset-lg-3">
            <div class="row mt-5">
                @foreach ($project->applicants as $applicant)
                <div class="col-lg-4 mb-5">
                    <div class=" mb-5 card {{ $applicant->pivot->status ? "bg-secondary text-white" : "" }}">
                        <div class="card-body">
                            {{-- {{ dd(['project' => $applicant->id])}} --}}
                            @if ($applicant->avatar)

                            <a href="{{ route('profiles.show', ['profile' => $applicant->id]) }}"><img class="img-thumbnail1" src="{{  url('assets/profile/'.$applicant->avatar) }}" alt="User image"></a>
                        @else
                        <a href="{{ route('profiles.show', ['profile' => $applicant->id]) }}"><img class="img-thumbnail1"
                                src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png" alt="User image"></a>
                        @endif
                            <p class="h6 text-center font-weight-bold assembly-name mt-3">{{ $applicant->name }}</p>
                            <p class="h5 text-center assembly-proffesion">{{ $applicant->academy->profession}}</p>
                            <p class="h5 text-center assembly-email">{{ $applicant->email}}</p>
                            <p class="text-center h5 mt-4 ">{{ $applicant->pivot->message }}</p>
                        </div>
                        <div class="text-center">
                            <a href="{{ route('applicants.accept', ['project' => $project->id, 'user' => $applicant->id]) }}" class="btn {{ $applicant->pivot->status ? "disabled" : "" }}"><img src="{{ asset('icons/1.png') }}" alt="" width="30px"></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

</div>

</x-app-layout>
