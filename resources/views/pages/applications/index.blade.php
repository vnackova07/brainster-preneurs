<x-app-layout>

    <div class="row mt-5">
        <div class="col-lg-2 offset-lg-2">
            <div class="row justify-content-center">                
                <div class="col-6 mb-4">

                </div>
            </div>
        </div>
        <div class="col-lg-6">
            @foreach ($projects as $project)                
                @include('inc.project-card')
            @endforeach
        </div>
    </div>


    
</x-app-layout>
