<x-app-layout>
    <div class="container-fluid flex flex-row create">
    <div class="row mt-5">

<form action="{{ route('projects.store') }}" method="POST">
            @csrf
        <div class="col-lg-9 left">
            <div class="row">
                <div class="pull-left">
                    <h2 class="createtitle">New Project</h2>
                </div>
                <div class="mt-4 input left form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <input type="text" name="name"  class="block mt-1 w-full text-line form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required="true"  placeholder="Name of Project">
                    @if ($errors->has('name'))
                      <span class="error text-danger" 
                            id="name-error" 
                            for="input-name">{{ $errors->first('name') }}</span>

                    @endif
                </div>
                <div class="mt-4 mb-4 flex-column form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                    <strong class="createdesc">Description of project</strong><br>
                    <textarea class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }} createtextarea" rows="10" cols="65" name="description" placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."></textarea>

                    @if ($errors->has('description'))
                      <span class="error text-danger" 
                            id="description-error" 
                            for="input-description">{{ $errors->first('description') }}</span>

                    @endif
                </div>
            </div>
        </div>
    </div>
       
        <div class="col-lg-5 academies right">
            <div class="row mt-5">
            <h1 class="createtitle">What I need</h1>    
            
                <div class="form-group{{ $errors->has('academies') ? ' has-danger' : '' }}">
                    <div class="input-group-text academiescheck ">
                  @foreach ($academies as $item)
                    <input type="checkbox" name="academies[]" value="{{ $item->id }}" id="sizeschck{{ $item->id }}" aria-label="Checkbox for following text input">
                      
                    <label class="btn-default" style="cursor: pointer;" for="sizeschck{{ $item->id }}">{{ $item->name }}</label>
                    
                  @endforeach
                </div>

                  @if ($errors->has('academies'))
                    <span class="error text-danger" 
                          id="academies-error" 
                          for="input-academies">{{ $errors->first('academies') }}</span>

                  @endif
                </div>

                <p class="yellowp">Please select no more than 4 options</p>

                 {{-- Footer Submit Save Button --}}
                <div class=" ml-auto mr-auto editbtn">
                    <button type="submit" class="btn btn-primary m-2 edit">{{ __('EDIT') }}</button>
                {{-- <a href="{{ route('projects.index') }}" class="btn m-2 edit">{{ __('EDIT') }}</a> --}}
                </div>
        </div>
        </div>
    
</form>
</div>
    </div>




</x-app-layout>

<script>
    $(function(){
        
        $('.academiescheck label').each(function(){
            $(this).on('click', () => {
                if($(this).hasClass('btn-default')){
                    console.log("Hello")
                    $(this).addClass('btn-selected')
                    $(this).removeClass('btn-default')
                }else{
                    $(this).removeClass('btn-selected')
                    $(this).addClass('btn-default')
                }
            })
        })
    })
</script>


