<x-app-layout>
    <div class="col-6 mb-4 projecttitle">
        <h2>Have a new idea to make the world better?</h2>
        <span class="d-flex">Create new project<a href="{{ route('projects.create') }}"><img src="../icons/1.png" alt="Plus" class="w-25">
        </a ></span>
    </div>
    <div class="row justify-content-center">

    <div class="row mt-5">

        </div>
        <div class="col-lg-8">
            @foreach ($projects as $project)
                @include('inc.project-card')
            @endforeach
        </div>
    </div>



</x-app-layout>

<script>
    $('.show-more').on('click', function(){
        $('.showmorediv').css("max-height" , "initial");
        });

    </script>
