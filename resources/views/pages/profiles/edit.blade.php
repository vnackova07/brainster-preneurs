<x-app-layout>
    <form action="{{ route('profiles.update', ['profile' => $user->id]) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
        @csrf
        @method('put')

<div class="myprofile">
    <div class="profile-left prof-row">
        <div class="prof-title">
            <h1 class="h3">My Profile</h1>
        </div>

        <div class="profile-details d-flex mt-3">
            <div class="detail-left half mt-5">
                <div class="pic mt-5">
                @if ($user->avatar)
                    <img class="img-thumbnail" name="avatar" width="90px" src="{{ url('assets/profile/'.$user->avatar) }}" alt="User image">
                @else
                    <img class="img-thumbnail" width="90px"
                        src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png" alt="User image">
                @endif

                    <input type="file" name="img" id="inp-pic" hidden>
                    <label for="inp-pic" style="cursor : pointer;" class="mt-4">Click here to upload an image</label>
                </div>

            </div>

            <div class="detail-right half">
                <div class="prof-name margin-prof">
                <div class="mt-4 mb-0 input left form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <input type="text" name="name"  class="block mt-1 w-full text-line form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required="true" value="{{ $user->name }}"   placeholder="Name">
                    @if ($errors->has('name'))
                      <span class="error text-danger"
                            id="name-error"
                            for="input-name">{{ $errors->first('name') }}</span>

                    @endif
                </div>
                </div>

                <div class="prof-name">
                    <div class="mt-1 mb-0 input left form-group{{ $errors->has('surname') ? ' has-danger' : '' }}">
                        <input type="text" name="surname"  class="block mt-1 w-full text-line form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" required="true" value="{{ $user->surname }}"   placeholder="Surname">
                        @if ($errors->has('surname'))
                          <span class="error text-danger"
                                id="surname-error"
                                for="input-surname">{{ $errors->first('surname') }}</span>

                        @endif
                    </div>
                    </div>
                    <div class="prof-name">
                        <div class="mt-1 mb-0 input left form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <input type="text" name="email"  class="block mt-1 w-full text-line form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required="true" value="{{ $user->email }}"   placeholder="Email">
                            @if ($errors->has('email'))
                              <span class="error text-danger"
                                    id="email-error"
                                    for="input-email">{{ $errors->first('email') }}</span>

                            @endif
                        </div>
                        </div>
            </div>
        </div>

        {{-- Biography edit --}}
        <div class="prof-detail mt-5">
        <div class="mt-4 mb-4 ml-0 flex-column form-group{{ $errors->has('biography') ? ' has-danger' : '' }}">
        <strong class="createdesc">Biography</strong><br>
        <textarea class="form-control {{ $errors->has('biography') ? ' is-invalid' : '' }} createtextarea"  name="biography" placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.">{{ $user->biography }}</textarea>

        @if ($errors->has('biography'))
        <span class="error text-danger"
        id="biography-error"
        value="{{ $user->biography }}"
        for="input-biography">{{ $errors->first('biography') }}</span>

        @endif

        </div>
    </div>

    </div>

    <div class="profile-right prof-row">
        <div class="prof-title">
            <h1 class="h3">Skills</h1>
            <div class="prof-skills">
                <div class="input-group-text skillscheck">
                    @foreach ($skills as $item)

                        @php($is = true)

                          @foreach ($user->skills as $skill)

                          @if ($skill->pivot->skill_id == $item->id)

                            <input type="checkbox" name="skills[]" value="{{ $item->id }}" id="{{ $item->id }}" aria-label="Checkbox for following text input" checked class="d-none">

                            <label class="btn-selected2 skill" style="cursor: pointer;" for="{{ $item->id }}">{{ $item->name }}</label>

                              @php($is = false)
                              @break
                          @endif
                          @endforeach



                        @if($is)
                        <input type="checkbox" name="skills[]" value="{{ $item->id }}" id="{{ $item->id }}" aria-label="Checkbox for following text input" class="d-none">
                        <label class="btn-default2 skill" style="cursor: pointer;" for="{{ $item->id }}">{{ $item->name }}</label>
                        @endif

                    @endforeach


                        </div>
                        @if ($errors->has('skills'))
                        <span class="error text-danger mt-3"
                              id="skills-error"
                              for="input-skills">{{ $errors->first('skills') }}</span>

                      @endif
            </div>
        </div>

        <div class="academies mb-5">
            <div class="prof-title mb-0 mt-0">
                <h1 class="h3 mb-0 mt-3">Academies</h1></div>
            <div class="input-group-text academieprofscheck">
                @foreach ($academies as $itemm)

                    @php($is = true)
                    {{-- {{ dd($user->academy)}} --}}
                      @foreach ($user->academy as $academy)
                      {{-- {{ dd($academy->pivot->academy_id)}} --}}
                      @if ($user->academy_id == $itemm->id)

                        <input type="checkbox" name="academies[]" value="{{ $itemm->id }}" id="{{ $itemm->id }}" aria-label="Checkbox for following text input" checked class="d-none">

                        <label class="btn-selected prof-academy-select" style="cursor: pointer;" for="{{ $itemm->id }}">{{ $itemm->name }}</label>

                          @php($is = false)
                          @break
                      @endif
                      @endforeach



                    @if($is)
                    <input type="checkbox" name="academies[]" value="{{ $itemm->id }}" id="{{ $itemm->id }}" aria-label="Checkbox for following text input" class="d-none">
                    <label class="btn-default prof-academy-select" style="cursor: pointer;" for="{{ $itemm->id }}">{{ $itemm->name }}</label>
                    @endif

                @endforeach

                      @if ($errors->has('academies'))
                        <span class="error text-danger"
                              id="academies-error"
                              for="input-academies">{{ $errors->first('academies') }}</span>

                      @endif
                    </div>
        </div>

          {{-- Footer Submit Save Button --}}
          <div class=" ml-auto mr-auto mt-0 editbtn">
            <button type="submit" class="btn btn-primary m-2 mt-0 edit">{{ __('EDIT') }}</button>
        {{-- <a href="{{ route('projects.index') }}" class="btn m-2 edit">{{ __('EDIT') }}</a> --}}
        </div>
    </div>



</div>

</div>
    </form>
</x-app-layout>
<script>
    $(function(){

        $('.skill').each(function(){
            $(this).on('click', () => {
                if($(this).hasClass('btn-default2')){
                    console.log("Hello")
                    $(this).addClass('btn-selected2')
                    $(this).removeClass('btn-default2')
                }else{
                    $(this).removeClass('btn-selected2')
                    $(this).addClass('btn-default2')
                }
            })
        })
    })



    $(function() {
        $('.prof-academy-select').on('click', function(e) {
            $.each($('.prof-academy-select'), function(e) {
                $(this).removeClass('btn-selected')
                $(this).addClass('btn-default');
            });

            $(this).removeClass('btn-default')
            $(this).addClass('btn-selected')
        });
    })





</script>
