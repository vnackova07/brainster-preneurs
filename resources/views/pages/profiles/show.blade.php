<x-app-layout>
<div class="container-fluid myprofile">

    <div class="profile-left prof-row">
        <div class="profile-details d-flex mt-3">
            <div class="detail-left half mt-5">
        <div class="pic mt-5">
            @if ($user->avatar)
                <img class="img-thumbnail" name="avatar" width="90px" src="{{ url('assets/profile/'.$user->avatar) }}" alt="User image">
            @else
                <img class="img-thumbnail" width="90px"
                    src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png" alt="User image">
            @endif

            </div>
        </div>
        <div class="detail-right half mt-5">
            <div class="h4 graytitle">Name</div>
            <div class="h3 boldtitle">{{ $user->name ." " . $user->surname}}</div>
            <div class="h4 graytitle">Contact</div>
            <div class="h5 boldtitle">{{ $user->email}}</div>
        </div>
    </div>


    </div>
    <div class="profile-right prof-row mt-5">
        <div><span class="h4 graytitle">Biography</span>
        <p class="showmore mt-4 graybio">{{ $user->biography}}</p></div>

        <div class="prof-title mt-5">
        <h1 class="h3">Skills</h1>
            <div class="prof-skills">
                <div class="input-group-text skillsshow">

                          @foreach ($user->skills as $skill)

                            <input type="checkbox" name="skills[]" value="{{ $skill->id }}" id="{{ $skill->id }}" aria-label="Checkbox for following text input" class="d-none">

                            <label class="btn-default skill" style="cursor: pointer;" for="{{ $skill->id }}">{{ $skill->name }}</label>


                            @endforeach
                        </div>

            </div>
        </div>
    </div>

</div>


</x-app-layout>
<script>
document.querySelectorAll('.showmore').forEach(element => {

    if (element.outerText.length > 500) {
        let span = document.createElement('span')
        span.className = 'show t-orange'
        span.innerText = 'show more'
        element.parentNode.appendChild(span)

        let tempText = String(element.outerText)
        element.innerText = String(element.innerText).slice(0, 500)
        $(span).on('click', () => {
            element.innerText = tempText
            $(span).remove()
        })
    }
});
</script>
