<?php

namespace Database\Seeders;

use App\Models\Academy;
use App\Models\Project;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create users with projects
        User::factory()
        ->has(Project::factory()->count(3))        
        ->count(10)
        ->create();


        $users = User::all();
        foreach($users as $user) {
            // Attach skills to each user
            foreach(Skill::inRandomOrder()->get()->take(rand(5, 10)) as $skill) {
                $user->skills()->attach($skill);
            }
        }


        $projects = Project::all();
        foreach($projects as $project){
            //Attach project requirement to each project
            foreach(Academy::inRandomOrder()->get()->take(2) as $academy) {
                $project->requirements()->attach($academy);
            }
        }


        foreach($users as $user) {
            // Attach projects to each user
            foreach(Project::where('user_id', '!=', $user->id)->inRandomOrder()->get()->take(6) as $project) {
                $user->applications()->attach($project, ['message' => 'Choose me, I am very good', 'status' => false]);
            }
        }

    }
}
