<?php

namespace Database\Seeders;

use App\Models\Academy;
use Illuminate\Database\Seeder;

class AcademySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $academies = [
            ['name' => 'Full Stack', 'profession' => 'Full Stack Developer'],
            ['name' => 'Front End', 'profession' => 'Front End Developer'],
            ['name' => 'Marketing', 'profession' => 'Marketing Agent'],
            ['name' => 'Design', 'profession' => 'Designer'],
            ['name' => 'Data Science', 'profession' => 'Data Scientist'],
            ['name' => 'QA', 'profession' => 'Quality Assurance Tester'],
            ['name' => 'UX/UI', 'profession' => 'UX/UI Designer'],
        ];
        foreach($academies as $academy) {
            Academy::create([
                'name' => $academy['name'],
                'profession' => $academy['profession'],
            ]);
        }
    }
}
