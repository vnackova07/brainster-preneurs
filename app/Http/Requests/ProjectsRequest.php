<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "description" => "required",
            "academies" => "required|array|min:1|max:4"
        ];
    }

    public function messages()
    {
        return [
            "name.required" => 'Name is required.',
            "description.required" => 'Description is required.',
            "academies.required" => 'Academies is required.',
            "academies.array" => 'Academies must be array.',
            "academies.min:1" => 'Academies must be at least 1 item long.',
            "academies.max:1" => 'Academies must be at least 4 item long.',
        ];
    }
}
