<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfilesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "surname" => "required",
            "email" => "required|email|",
            "biography" => "required",
            "academies" => "required|array|min:1|max:1",
            "skills" => "required|array|min:5|max:10"
        ];
    }

    public function messages()
    {
        return [
            "name.required" => 'Name is required.',
            "surname.required" => 'Surname is required.',
            "email.required" => 'Email is required.',
            "email.email" => 'Email need to be in valid format.',
            "biography.required" => 'Biography is required.',
            "academies.required" => 'Academies are required.',
            "academies.array" => 'Academies must be array.',
            "academies.min:1" => 'Academies must be at least 1 item long.',
            "academies.max:1" => 'Academies must be at least 1 item long.',
            "skills.required" => 'Skills are required.',
            "skills.array" => 'Skills must be array.',
            "skills.min:5" => 'Skills must be at least 5 item long.',
            "skills.max:10" => 'Skills must be at least 10 item long.',
        ];
    }
}
