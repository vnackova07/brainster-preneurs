<?php

namespace App\Http\Controllers;

use App\Models\Academy;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectRequirement;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProjectsRequest;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with('user', 'user.academy', 'applicants')
            ->where('user_id', auth()->user()->id)
            ->latest()
            ->get();

        return view('pages.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $academies = Academy::all('id', 'name');
        return view('pages.projects.create', compact('academies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectsRequest $request)
    {

        $project = new Project();
        $project->name = $request->name;
        $project->description = $request->description;
        $project->user_id = Auth::id();
        $project->save();

        // List all arrays input
        $input['academies'] = $request->input('academies');

        $selectProjectId = Project::orderBy('id', 'desc')->first('id');
        $id = $selectProjectId->id;

        // Create academies
        foreach ($input['academies'] as $academy) {
            ProjectRequirement::create([
                'project_id' => $id,
                'academy_id' => $academy
            ]);
        }

        return redirect()->route('projects.index')->with('status', 'Project Created');

        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('pages.projects.edit', ['model' => Project::where('id', $id)->first(), 'academies' => Academy::all(), 'requirements' => ProjectRequirement::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectsRequest $request, $id)
    {
        $project = Project::find($id);
        $project->name = $request->name;
        $project->description = $request->description;
        $project->user_id = Auth::id();

        // List all arrays input
        $input['academies'] = $request->input('academies');


        $project->requirements()->sync($input['academies']);


        $project->update();

        return redirect()->route('projects.index')->with('status', 'Project Created');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);


        $project->applicants()->detach();
        $project->requirements()->detach();

        $project->delete();

        return redirect()->route('projects.index')
            ->withSuccess(__('Post delete successfully.'));
    }

    public function getProjects(Request $request)
    {
        $projects = Project::with('user', 'user.academy', 'applicants')
            ->where('user_id', '!=', auth()->user()->id)
            ->when($request->has('academy_id'), function ($query) use ($request) {
                $query->whereHas('requirements', function ($query) use ($request) {
                    $query->where('academy_id', $request->academy_id);
                });
            })
            ->latest()
            ->paginate(3)
            ->withQueryString();

        return view('inc.projects', compact('projects'));
    }
}
