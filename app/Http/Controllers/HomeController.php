<?php

namespace App\Http\Controllers;

use App\Models\Academy;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        
        $academies = Academy::all('id', 'name');
        $user = User::find('$id');

        
        return view('dashboard', compact('academies', 'user'));
    }
}

