<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Skill;
use App\Models\Academy;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectRequirement;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProfilesRequest;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Project::with('user', 'user.skills')
            ->where('user_id', auth()->user()->id)
            ->latest()
            ->get();

        return view('pages.profiles.index', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.profiles.show', ['user' => User::where('id', $id)->first(), 'skills' => Skill::all(),'project' => Project::where('id', $id)->first()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.profiles.edit', ['user' => User::where('id', $id)->first(), 'skills' => Skill::all(), 'academies' => Academy::all(), 'requirements' => ProjectRequirement::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfilesRequest $request, $id)
    {
        $userAvatar = User::where('id', Auth::id())->first();
        $id = Auth::id();


        if($request->hasFile('img') || $userAvatar->avatar == null){

            $request->validate([

                'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

            if($userAvatar->avatar != null) {

                unlink(public_path("assets/profile/". $userAvatar->avatar));

            }

            $file = $request->file('img');

            $extension = $file->getClientOriginalExtension(); // getting image extension

            $filename =time().'.'.$extension;

            $file->move('assets/profile/', $filename);

            User::where('id', $id)->update([

                'avatar' => $filename,
            ]);

        }

        User::where('id', $id)->update([

            'name' => $request->input('name'),
            'surname' => $request->input('surname'),
            'email' => $request->input('email'),
            'biography' => $request->input('biography'),
            ]);

        // List all arrays input
        $input['skills'] = $request->input('skills');
        // $input['academies'] = $request->input('academies');

        $userAvatar->skills()->sync($input['skills']);
        $userAvatar->academy_id = intval($request->input('academies'));

        $userAvatar->save();

        if($userAvatar->name != null && $userAvatar->surname != null && $userAvatar->email != null && $userAvatar->biography != null && $userAvatar->avatar != null && $userAvatar->skills() !=null && $userAvatar->academy_id !=null){
            User::where('id', $id)->update([

                'profile_complete' => 1
            ]);
        }


        return redirect()->route('profiles.edit', ['profile' => auth()->id()])->with('status', 'Project Created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
