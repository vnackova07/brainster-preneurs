<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Academy extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function users()
    {
        return $this->hasMany(User::class, 'academy_id', 'id');
    }

    public function requirements()
    {
        return $this->belongsToMany(Project::class, 'project_requirements', 'academy_id', 'project_id');
    }
}
