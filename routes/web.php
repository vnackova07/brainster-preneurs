<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ApplicantController;
use App\Http\Controllers\ApplicationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/dashboard',                            [HomeController::class, 'index'])->middleware(['auth', 'verified'])->name('dashboard');

Route::resource('/applications',                    ApplicationController::class)->middleware('auth');
Route::resource('/applicants',                      ApplicantController::class)->except('index')->middleware('auth');
Route::resource('/projects',                        ProjectController::class)->middleware('auth');
Route::resource('/profiles',                         ProfileController::class)->middleware('auth');

Route::get('projects/{project}/applicants/',        [ApplicantController::class, 'index'])->name('applicants.index')->middleware('auth');
Route::get('projects/{project}/applicants/{user}',  [ApplicantController::class, 'acceptApplicant'])->name('applicants.accept');
Route::get('projects/{project}/assembleTeam',       [ApplicantController::class, 'assembleTeam'])->name('applicants.assemble');
Route::get('/ajax/projects', [ProjectController::class, 'getProjects'])->name('ajax.projects');
require __DIR__.'/auth.php';
